package com.example.anthonyvisalli.calc_visalli;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by anthonyvisalli on 2/22/18.
 */

public class Listener extends AppCompatActivity implements View.OnClickListener
{
    private final int position;
    EditText et;
    Expression expr;
    String number;

    public Listener(int position, EditText et, Expression expr, String number)
    {
        this.position = position;
        this.et = et;
        this.expr = expr;
        this.number = number;
    }

    public void onClick(View view)
    {
        switch(this.position)
        {
            case 10:
                if (expr.getFullExpression().length() == 0 && expr.getDotCount() == 0) {
                    displayText(".");
                    expr.setDotCount(1);
                    break;
                }else if(expr.getFullExpression().charAt(expr.getFullExpression().length()-1) != '.' && expr.getDotCount() == 0) {
                    displayText(".");
                    expr.setDotCount(1);
                    break;
                }
            case 11:
                expr.clearFullExpression();
                expr.clearNumber();
                et.setText("");
                break;
            case 9:
                displayText("0");
                break; 
            default:
                displayText(Integer.toString(this.position + 1));
                break;
        }
    }

    private void displayText(String s) {
        expr.setNumber(s);
        et.setText(expr.getFullExpression());
        //et.setText(expr.getNumber());
    }
}
