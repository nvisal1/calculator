package com.example.anthonyvisalli.calc_visalli;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;



/**
 * Created by anthonyvisalli on 2/22/18.
 */

public class ButtonAdapter extends BaseAdapter implements View.OnClickListener {
    private Context mContext;
    EditText et;
    Expression expr;
    String number;

    public ButtonAdapter(Context c, EditText et, Expression expr, String number) {
        mContext = c;
        this.et = et;
        this.expr = expr;
        this.number = number;
    }

    @Override
    public int getCount() {
        return numbers.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Button btn;
        if (view == null) {
            btn = new Button(mContext);
            btn.setBackgroundColor(Color.parseColor("#00FFFF"));
            btn.setLayoutParams(new GridView.LayoutParams(230, 250));
            btn.setPadding(8, 8, 8, 8);
            btn.setTextSize(25);
        }
        else {
            btn = (Button) view;
        }

        btn.setText(numbers[i]);
        btn.setTextColor(Color.BLACK);
        //btn.setBackgroundResource(R.drawable.button);
        btn.setId(i);

        btn.setOnClickListener(new Listener(i, et, expr, number));
        return btn;

    }

    public String[] numbers = {
            "1", "2", "3" , "4", "5" , "6", "7", "8", "9" , "0", ".", "clear"
    };

    @Override
    public void onClick(View view) {

    }
}


