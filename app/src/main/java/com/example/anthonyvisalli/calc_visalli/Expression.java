package com.example.anthonyvisalli.calc_visalli;

import java.util.Arrays;

/**
 * Created by anthonyvisalli on 2/19/18.
 */

public class Expression {
    private String number;
    private String operator;
    private String fullExpression;
    private String value;
    int valueCount;
    int startPosition;
    int dotCount;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
            this.number = number;
            this.setFullExpression(this.number);
    }

    public void setValue() {
        if (getFullExpression() != "" && startPosition < getFullExpression().length()) {
            this.value = this.getFullExpression().substring(startPosition, getFullExpression().length() - 1);
        }
    }

    private void setFullExpression(String expression) {
        this.fullExpression = this.fullExpression + expression;
    }

    public String getFullExpression() {
        return this.fullExpression;
    }

    public void clearFullExpression() {
        this.fullExpression = "";
        this.setStartPosition(0);
        this.clearOperator();
        this.setDotCount(0);
    }

    public void clearNumber() {this.number = "";}

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        if (this.operator == "") {
            this.operator = operator;
            if (getFullExpression() != "") {
                this.setFullExpression(this.operator);
            }
        }
    }

    public void clearOperator () {
        this.operator = "";
    }

    public int getValueCount() {
        return valueCount;
    }

    public String getValue() {
        return value;
    }

    public void setStartPosition (int i) {
        this.startPosition = i;
    }

    public void setDotCount (int i) {
        this.dotCount = i;
    }

    public int getDotCount () {
        return this.dotCount;
    }

    public Expression() {
        this.number = "";
        this.operator = "";
        this.fullExpression = "";
        this.valueCount = 0;
        this.startPosition = 0;
        this.value = "";
        this.dotCount = 0;
    }

    public void clearValue() {
        this.value = "";
    }
}
