package com.example.anthonyvisalli.calc_visalli;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

public class Calculator extends AppCompatActivity implements View.OnClickListener {

    EditText editInput;
    Button plus;
    Button minus;
    Button multiply;
    Button divide;
    Button equal;
    String number = "";
    Expression expr = new Expression();
    String num1;
    String num2;
    double resultD;
    int resultI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        editInput = findViewById(R.id.enterCalc);

        plus = findViewById(R.id.plusBtn);
        plus.setOnClickListener(this);

        minus = findViewById(R.id.minusBtn);
        minus.setOnClickListener(this);

        multiply = findViewById(R.id.multiplyBtn);
        multiply.setOnClickListener(this);

        divide = findViewById(R.id.divideBtn);
        divide.setOnClickListener(this);

        equal = findViewById(R.id.equalsBtn);
        equal.setOnClickListener(this);

        setUpGrid();

    }

    private void setUpGrid() {
        GridView gridview = findViewById(R.id.gridview);
        gridview.setAdapter(new ButtonAdapter(this, editInput, expr, number));
    }

    @Override
    public void onClick(View view) {
        switch(view.getId())
        {
            case R.id.plusBtn:
                expr.setOperator(plus.getText().toString());
                expr.clearValue();
                expr.setValue();
                num1 = expr.getValue();
                expr.setDotCount(0);
                displayText(expr.getFullExpression());
                expr.setStartPosition(expr.getFullExpression().length());
                break;
            case R.id.minusBtn:
                expr.setOperator(minus.getText().toString());
                expr.clearValue();
                expr.setValue();
                num1 = expr.getValue();
                expr.setDotCount(0);
                displayText(expr.getFullExpression());
                expr.setStartPosition(expr.getFullExpression().length());
                break;
            case R.id.multiplyBtn:
                expr.setOperator(multiply.getText().toString());
                expr.clearValue();
                expr.setValue();
                num1 = expr.getValue();
                expr.setDotCount(0);
                displayText(expr.getFullExpression());
                expr.setStartPosition(expr.getFullExpression().length());
                break;
            case R.id.divideBtn:
                expr.setOperator(divide.getText().toString());
                expr.clearValue();
                expr.setValue();
                num1 = expr.getValue();
                expr.setDotCount(0);
                displayText(expr.getFullExpression());
                expr.setStartPosition(expr.getFullExpression().length());
                break;
            case R.id.equalsBtn:
                expr.setNumber("=");
                expr.clearValue();
                expr.setValue();
                num2 = expr.getValue();
                calculate();
                //expr.setOperator("");
                expr.clearFullExpression();
               // displayText(equal);
                break;
        }
    }

    private void calculate() {
        String s = expr.getFullExpression() + "=";
        displayText(s);

        switch (expr.getOperator()) {
            case "+":
                if (num1 != "" && num2 != "") {
                    if (s.contains(".")) {
                        resultD = Double.parseDouble(num1) + Double.parseDouble(num2);
                        displayText(String.valueOf(resultD));
                    }
                    else {
                        resultI = Integer.parseInt(num1) + Integer.parseInt(num2);
                        displayText(String.valueOf(resultI));
                    }
                }
                break;
            case "-":
                if (num1 != "" && num2 != "") {
                    if (s.contains(".")) {
                        resultD = Double.parseDouble(num1) - Double.parseDouble(num2);
                        displayText(String.valueOf(resultD));
                    }
                    else {
                        resultI = Integer.parseInt(num1) - Integer.parseInt(num2);
                        displayText(String.valueOf(resultI));
                    }
                }
                break;
            case "*":
                if (num1 != "" && num2 != "") {
                    if (s.contains(".")) {
                        resultD = Double.parseDouble(num1) * Double.parseDouble(num2);
                        displayText(String.valueOf(resultD));
                    }
                    else {
                        resultI = Integer.parseInt(num1) * Integer.parseInt(num2);
                        displayText(String.valueOf(resultI));
                    }
                }
                break;
            case "/":
                if (num2.equals("0")) {
                    displayText("Error: division by 0");
                } else {
                    if (num1 != "" && num2 != "") {
                        if (s.contains(".")) {
                            resultD = Double.parseDouble(num1) / Double.parseDouble(num2);
                            displayText(String.valueOf(resultD));
                        }
                        else {
                            resultI = Integer.parseInt(num1) / Integer.parseInt(num2);
                            displayText(String.valueOf(resultI));
                        }
                    }
                }
                break;
        }

    }

    private void displayText(String string) {
        editInput.setText(string);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
